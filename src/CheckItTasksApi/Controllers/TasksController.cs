using CheckItTasksApi.Models;
using Microsoft.AspNetCore.Mvc;

namespace CheckItTasksApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class TasksController : ControllerBase
    {
        private static readonly string[] Summaries = new[]
        {
        "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        private readonly ILogger<TasksController> _logger;

        public TasksController(ILogger<TasksController> logger)
        {
            _logger = logger;
        }

        [HttpGet]
        public ActionResult<IEnumerable<Models.Task>> GetTasks(int? userId = null, string? tags = null, bool orderByRating = false)
        {
            return Ok(new List<Models.Task>());
        }

        [HttpGet("{id}")]
        public ActionResult<Models.Task> GetTask(int id)
        {
            return Ok(new Models.Task());
        }

        [HttpPost()]
        public ActionResult<Models.Task> CreateTask()
        {
            return Ok();
        }

        [HttpPut("{id}")]
        public ActionResult<Models.Task> UpdateTask(Models.Task task)
        {
            return Ok();
        }

        [HttpPatch("{id}/status")]
        public ActionResult<Models.Task> UpdateStatus(int id, Models.TaskStatus taskStatus)
        {
            return Ok();
        }

        [HttpDelete("{id}")]
        public ActionResult DeleteTask(int id)
        {
            return Ok();
        }

        [HttpGet("{id}/solutions")]
        public ActionResult<IEnumerable<Solution>> GetSolutions(int id)
        {
            return Ok();
        }

        [HttpPost("{id}/solutions")]
        public ActionResult<Models.Solution> CreateSolution(int id, Solution solution)
        {
            return Ok();
        }

        [HttpPost("{id}/solutions/{solutionId}/feedback")]
        public ActionResult<Models.Feedback> CreateFeedback(int id, int solutionId, Feedback feedback)
        {
            return Ok();
        }

        [HttpPost("{id}/solutions/{solutionId}/feedback/cancel")]
        public ActionResult<Models.Feedback> CancelFeedback(int id, int solutionId)
        {
            return Ok();
        }

        [HttpGet("{id}/solutions/{solutionId}/feedback")]
        public ActionResult<Models.Feedback> GetFeedback(int id, int solutionId)
        {
            return Ok();
        }

        [HttpPatch("{id}/solutions/{solutionId}/feedback/rate")]
        public ActionResult<Models.Feedback> RateFeedback(int id, int solutionId, int rate)
        {
            return Ok();
        }
    }
}