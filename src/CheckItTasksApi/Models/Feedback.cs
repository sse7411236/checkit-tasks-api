﻿namespace CheckItTasksApi.Models
{
    public class Feedback : ModelBase
    {
        public string Text { get; set; }
        public DateTime CreatedUtc { get; set; }

        public int StudentRate { get; set; }

        public int SolutionId { get; set; }
        public Solution Solution { get; set; }

        // Transaction root reference
        public int TransactionId { get; set; }
    }
}
