﻿namespace CheckItTasksApi.Models
{
    public class Solution : ModelBase
    {
        public string Text { get; set; }
        public DateTime CreatedUtc { get; set; }
        public int StatusId { get; set; }
        public SolutionStatus Status { get; set; }
        public int TaskId { get; set; }
        public Task Task { get; set; }

        public int FeedbackId { get; set; }
        public Feedback Feedback { get; set; }

        // User root reference
        public int UserId { get; set; }

        // Transaction root reference
        public int TransactionId { get; set; }
    }
}
