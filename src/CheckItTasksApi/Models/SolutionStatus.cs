﻿namespace CheckItTasksApi.Models
{
    public class SolutionStatus : ModelBase
    {
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
