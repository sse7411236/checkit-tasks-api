﻿namespace CheckItTasksApi.Models
{
    public class Task : ModelBase
    {
        public string Name { get; set; }
        public string Text { get; set; }
        public string TaskTags { get; set; }
        public decimal CurrentPrice { get; set; }
        public int StatusId { get; set; }
        public TaskStatus Status { get; set; }
        public DateTime CreatedUtc { get; set; }
        public DateTime UpdatedUtc { get; set; }

        public List<Solution> Solutions { get; set; }

        // User root reference
        public int UserId { get; set; }
    }
}
