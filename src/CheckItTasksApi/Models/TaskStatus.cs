﻿namespace CheckItTasksApi.Models
{
    public class TaskStatus : ModelBase
    {
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
